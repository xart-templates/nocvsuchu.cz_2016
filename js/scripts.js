jQuery(document).ready(function($){

	$('html').addClass('js');

	$('.box_accordion .item').each(function(){
		var item = $(this);
		$('.header a',this).click(function(event){
			event.preventDefault();
			$(item).toggleClass('on');
		});
	});

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

});